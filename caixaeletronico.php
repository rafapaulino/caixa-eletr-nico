<?php

class CaixaEletronico
{
	private $notas = array(100, 50, 20 ,10);
    private $dinheiro = array();
    private $retorno = array();

	public function saque($valor)
	{
        //verificando se o valor é um número
        if (!is_numeric($valor)) {
            $this->retorno = array(0 => 0);
        //verificando se o valor é maior que zero
        } elseif ($valor <= 0) {
            $this->retorno = array("Error" => "O valor solicitado é inválido!");
        //verificando se e multiplo de 10
        } elseif($valor % 10 != 0) {
            $this->retorno = array("Error" => "Não é possível sacar o valor solicitado. Temos apenas notas disponíveis nos valores de ".implode(", ",$this->notas)." reais!");
        //fazendo a logica para o saque
        } else {
            foreach($this->notas as $cedula)
            {
                while($cedula <= $valor)
                {
                    array_push($this->dinheiro, $cedula);
                    $valor -= $cedula;
                }
            }
            //agrupa os valores pela quantidade de notas utilizadas
            $this->retorno = array_count_values($this->dinheiro);
        }
        return json_encode($this->retorno);
	}
}