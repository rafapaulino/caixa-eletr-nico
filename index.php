<?php
require 'caixaeletronico.php';

    if ($_SERVER['REQUEST_METHOD'] == "POST" && count($_POST) > 0 && isset($_POST['valor']) ) {
        header('Content-Type: application/json');
        $caixa = new CaixaEletronico();
        echo $caixa->saque($_POST['valor']);
        exit;
    }
?>
<!DOCTYPE html>
<html lang="pt_BR">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Caixa Eletrônico</title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">

    <!-- CSS -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.4.1/css/alertify.min.css"/>
    <!-- Bootstrap theme -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.4.1/css/themes/bootstrap.min.css"/>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<br><br>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Caixa Eletrônico - RafaBank</h3>
                    </div>
                    <div class="panel-body">
                       <p>Informe o valor desejado no campo abaixo, para atendimento ligue no número 0800-100-505.</p>

                        <form class="form-inline" method="post" action="index.php" id="formulario">
                            <div class="form-group col-xs-6">
                                <label class="sr-only" for="valor">Valor do Saque:</label>
                                <div class="input-group col-xs-12">
                                    <div class="input-group-addon">R$</div>
                                    <input type="text" class="form-control" id="valor" placeholder="Digite o valor que você deseja sacar..." required="required">
                                    <div class="input-group-addon">.00</div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-success">Efetuar o Saque</button>
                        </form>
                    </div>
                 </div>
            </div>
        </div>
    </div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<!-- JavaScript -->
<script src="//cdn.jsdelivr.net/alertifyjs/1.4.1/alertify.min.js"></script>
<script>
;(function( window, document, $, undefined ) {
    'use strict';

    var caixaEletronico = (function() {

        var $public = {};

        $public.formulario = function() {
            $('#formulario').on('submit',function(event){
               event.preventDefault();

               //pegando os atributos para o envio do forulario
               var $this = $(this);
               var action = $this.attr('action');
               var valor = $('#valor').val();

                //pegando os resultados do waveet
                $.ajax({
                    url: action,
                    cache: false,
                    type: 'POST',
                    dataType: "json",
                    data: {
                        valor: valor,
                    },
                    success: function( data ) {
                        console.log(data);

                        if (data.Error === undefined && data[0] === undefined && data[0] !== 0 ) {

                            var message = ["Receba o seu dinheiro em:\r\n\r\n"];
                            $.each(data, function(k, v) {
                               message.push("Nota: "+k+" - Quantidade: "+v);
                            });
                            var alert = message.join("\r\n");
                            alertify.notify(alert, 'success', 20);

                        } else if (data[0] === 0) {
                            alertify.notify("Para de gracinha!", 'error', 10);
                        } else {
                            alertify.notify(data.Error, 'error', 10);
                        }
                    }//fim do success
                });//fim do ajax
            });
        }

        return $public;
    })();

    window.caixaEletronico = caixaEletronico;
    caixaEletronico.formulario();

})( window, document, jQuery );
</script>
</body>
</html>